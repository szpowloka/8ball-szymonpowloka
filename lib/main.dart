import 'package:flutter/material.dart';
import 'dart:math';

void main() => runApp(
      MaterialApp(
        home: Scaffold(
          backgroundColor: Colors.lightBlueAccent,
          appBar: AppBar(
            title: Center(child: Text('Ask me anything')),
            backgroundColor: Colors.blueAccent,
          ),
          body: BallPage(),
        ),
      ),
    );

class BallPage extends StatefulWidget {
  @override
  _BallPageState createState() => _BallPageState();
}

class _BallPageState extends State<BallPage> {
  var answer = 1;
  var buffor = 1;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: FlatButton(
            onPressed: () {
              while(buffor == answer){
                buffor = Random().nextInt(5) + 1;
              }
              setState(() {
                answer = buffor;
              });
            },
            child: Image.asset('images/ball$answer.png')),
      ),
    );
  }
}
